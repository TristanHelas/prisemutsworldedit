package de.prisemut.pwe.commands;

import org.bukkit.command.CommandSender;

public interface CommandInterface {

    void command(CommandSender commandSender, Command command, String label, String[] args);

    String name();

    String info();
}
