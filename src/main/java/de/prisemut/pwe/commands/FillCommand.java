package de.prisemut.pwe.commands;

import de.prisemut.pwe.Messages;
import de.prisemut.pwe.listener.PositionListener;
import de.prisemut.pwe.math.Cube;
import javafx.geometry.Pos;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FillCommand implements CommandInterface{


    public void command(CommandSender commandSender, Command command, String label, String[] args) {
        Player player = (Player)commandSender;
        Messages messages = new Messages(player);
        if(args.length == 2) {
            /*
            Cube part
             */
            Cube cube = new Cube(PositionListener.pos1.get(0), PositionListener.pos2.get(0), player);
            cube.fillCube(Material.getMaterial(args[1]));
            messages.sendMessage("Filled "+cube.getBlocks().size());
        } else {
            messages.sendMessage("Please use </pwe> <fc> <Material>!");
        }
    }

    public String name() {
        return "<fc> <Material>";
    }

    public String info() {
        return "Will fill your cube! You just need to marker two points with a carrot!";
    }
}
