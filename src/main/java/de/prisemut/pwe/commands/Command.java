package de.prisemut.pwe.commands;

import de.prisemut.pwe.Log;
import de.prisemut.pwe.Messages;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Command implements CommandExecutor {

    public static ArrayList<CommandInterface> cinterface = new ArrayList<CommandInterface>();
    public static ArrayList<String> name = new ArrayList<String>();
    public static ArrayList<String> info = new ArrayList<String>();

    public static void registerCommand(String c, CommandInterface ci) {
        cinterface.add(ci);
        name.add(ci.name());
        info.add(ci.info());
        Log l = new Log("Registerd command ["+c+"] successfully!");
        l.info();
    }

    public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] args) {
        Player player = (Player) commandSender;
        Messages m = new Messages(player);
        if(args[0].equals("help")) {
            m.sendMessage("===[PrisemutsWorldEdit]===");
            for(int i = 0; i < cinterface.size(); i++){
                m.sendMessage("["+name.get(i)+"] ~ "+info.get(i));
            }
            m.sendMessage("===[PrisemutsWorldEdit]===");
        } else if(args.length == 0) {
            m.sendMessage("===[PrisemutsWorldEdit]===");
            for(int i = 0; i < cinterface.size(); i++){
                m.sendMessage("["+name.get(i)+"] ~ "+info.get(i));
            }
            m.sendMessage("===[PrisemutsWorldEdit]===");
        }
        return false;
    }
}
