package de.prisemut.pwe;

import org.bukkit.entity.Player;

public class Messages {

    private Player player;

    private String prefix = "§7[§3PWE§7] ";

    public Messages(Player player) {
        this.player = player;
    }

    public void sendMessage(String msg) {
        player.sendMessage(prefix + msg);
    }
}
