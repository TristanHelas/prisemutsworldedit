package de.prisemut.pwe.listener;

import de.prisemut.pwe.Messages;
import de.prisemut.pwe.math.Cube;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;

public class PositionListener implements Listener {

    public static ArrayList<Location> pos1 = new ArrayList<Location>();
    public static ArrayList<Location> pos2 = new ArrayList<Location>();

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Messages m = new Messages(player);
        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && event.getItem().getType().equals(Material.CARROT) && event.getClickedBlock() != null) {
            pos2.clear();

            pos2.add(event.getClickedBlock().getLocation());
            m.sendMessage("Position 2 set!");
        } else if(event.getAction().equals(Action.LEFT_CLICK_BLOCK) && event.getItem().getType().equals(Material.CARROT) && event.getClickedBlock() != null) {
            event.setCancelled(true);
            pos1.clear();

            pos1.add(event.getClickedBlock().getLocation());
            m.sendMessage("Position 1 set!");
        }
    }

}
