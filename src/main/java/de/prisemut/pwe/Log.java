package de.prisemut.pwe;

public class Log {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private String msg;

    public Log(String message) {
        this.msg = message;
    }

    public void info() {
        System.out.println("["+ANSI_GREEN+"PWE INFO"+ANSI_WHITE+ "}"+msg);
    }

    public void err() {
        System.out.println("["+ANSI_RED+"PWE ERROR"+ANSI_WHITE+"] "+msg);
    }

}
