package de.prisemut.pwe;

import de.prisemut.pwe.commands.Command;
import de.prisemut.pwe.commands.FillCommand;
import de.prisemut.pwe.listener.PositionListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin {

    @Override
    public void onEnable() {
        super.onEnable();
        System.out.println("[PWE] Loading...");

        /*
        Registering listeners
         */
        System.out.println("[PWE] Loading Listener...");
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new PositionListener(),this);

        /*
        Command registering
         */
        System.out.println("[PWE] Loading Commands...");
        getCommand("pwe").setExecutor(new Command());
        Command.registerCommand("fc", new FillCommand());
    }

    @Override
    public void onDisable() {
        super.onDisable();
        System.out.println("[PWE] Unloading...");
    }
}
