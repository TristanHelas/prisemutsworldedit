package de.prisemut.pwe.math;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

public class Cylinder {

    private int radi;
    private Material material;
    private Location center;

    public Cylinder(int radius, Material material, Location spawn) {
        this.radi = radius;
        this.material = material;
        this.center = spawn;
    }

    public void performCylinder() {
        int x = center.getBlockX();
        int y = center.getBlockY();
        int z = center.getBlockZ();
        World w = center.getWorld();
        int squared = radi * radi;

        for(int cylx = x - radi; cylx <= x +radi; cylx++) {

            for(int cylz = z - radi; cylz <= z +radi; z++) {

                if((cylx - x) * (x - cylx) + (z - cylz) * (z - cylz) <= squared) {
                    w.getBlockAt(cylx, y, cylz).setType(material);
                }
            }
        }
    }
}
