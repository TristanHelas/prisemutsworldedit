package de.prisemut.pwe.math;

import de.prisemut.pwe.Messages;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class Cube implements Iterable<Block>, Cloneable{

    public Player player;
    private String worldName;
    private int x1, y1, z1;
    private int x2, y2, z2;

    public Cube(Location l1, Location l2, Player player) {
        Messages m = new Messages(player);
        if(l1.getWorld().equals(l2.getWorld()))  {
            this.player = player;
            this.worldName = l1.getWorld().getName();
            this.x1 = Math.min(l1.getBlockX(), l2.getBlockX());
            this.z1 = Math.min(l1.getBlockZ(), l2.getBlockZ());
            this.y1 = Math.min(l1.getBlockY(), l2.getBlockY());

            this.x2 = Math.max(l1.getBlockX(), l2.getBlockX());
            this.z2 = Math.max(l1.getBlockZ(), l2.getBlockZ());
            this.y2 = Math.max(l1.getBlockY(), l2.getBlockY());
        } else {
            m.sendMessage("Points must be in one world!");
        }
    }

    public int getUpper(String var) {
        if(var.equals("X")) {
            return this.x2;
        } else if(var.equals("Y")) {
            return  this.y2;
        } else if(var.equals("Z")) {
            return  this.z2;
        } else {
            return 0;
        }
    }

    public int getLower(String var) {
        if(var.equals("X")) {
            return this.x1;
        } else if(var.equals("Y")) {
            return  this.y1;
        } else if(var.equals("Z")) {
            return  this.z1;
        } else {
            return 0;
        }
    }

    public Location getCenter() {
        int x1 = this.getUpper("X") + 1;
        int z1 = this.getUpper("Z") + 1;
        int y1 = this.getUpper("Y") + 1;
        return new Location(Bukkit.getWorld(worldName), this.getLower("X") + (x1 - this.getLower("X")) / 2.0, this.getLower("Y") + (y1 - this.getLower("Y")) / 2.0, this.getLower("Z") + (z1 - this.getLower("Z") / 2.0));

    }

    public ArrayList<Block> getBlocks() {
        Iterator<Block> blockI = this.iterator();
        ArrayList<Block> copy = new ArrayList<Block>();
        while (blockI.hasNext())
            copy.add(blockI.next());
        return copy;
    }

    public void fillCube(Material material) {
        for(int i = 0; i < getBlocks().size(); i++) {
            getBlocks().get(i).setType(material);
        }
    }

    public Iterator<Block> iterator() {
        return new CubeIterator(Bukkit.getWorld(worldName), this.x1, this.y1, this.z1, this.x2, this.y2, this.z2);
    }

    public Map<String, Object> serialize() {
        return null;
    }

    public class CubeIterator implements Iterator<Block> {
        private World w;
        private int baseX, baseY, baseZ;
        private int x, y, z;
        private int sizeX, sizeY, sizeZ;

        public CubeIterator(World w, int x1, int y1, int z1, int x2, int y2, int z2) {
            this.w = w;
            this.baseX = x1;
            this.baseY = y1;
            this.baseZ = z1;
            this.sizeX = Math.abs(x2 - x1) + 1;
            this.sizeY = Math.abs(y2 - y1) + 1;
            this.sizeZ = Math.abs(z2 - z1) + 1;
            this.x = this.y = this.z = 0;
        }

        public boolean hasNext() {
            return this.x < this.sizeX && this.y < this.sizeY && this.z < this.sizeZ;
        }

        public Block next() {
            Block b = this.w.getBlockAt(this.baseX + this.x, this.baseY + this.y, this.baseZ + this.z);
            if (++x >= this.sizeX) {
                this.x = 0;
                if (++this.y >= this.sizeY) {
                    this.y = 0;
                    ++this.z;
                }
            }
            return b;
        }

        public void remove() {
        }
    }
}
